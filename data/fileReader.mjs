import fs from 'fs-extra';

let content;

async function getData(){
    content = await fs.readFile(process.env.INPUT_FILE, "utf8");
}

export default async function(){
    if (!content){
        await getData();
    } 

    if (!content){
        return "Ne peut pas lire le fichier"; 
    }

    return content;
}




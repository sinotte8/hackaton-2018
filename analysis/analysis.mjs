import sentiment from "./sentimentAnalysis.mjs";
import scrabble from "./scrabbleAnalysis.mjs";

export function singleWordAnalysis(word){
    return sentiment(word) + scrabble(word);
}

export function textAnalysis(text) {
    return sentiment(text) + scrabble(text);
}

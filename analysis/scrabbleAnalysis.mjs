import score from './scrabbleValue';
import removeAccent from 'remove-accents';
import removePunctuation from 'remove-punctuation';

function reduceFunction(accumulator, char) {
    let scoreObject = scoreList.find(x => x.letter == char);
    if(scoreObject !== undefined){
        accumulator += scoreObject.value;
    }
}

export default function(text){
    let scoreList = score();
    return removePunctuation(removeAccent(text)).split("").reduce(function (accumulator, char) {
        let scoreObject = scoreList.find(x => x.letter == char);
        if(scoreObject !== undefined){
            return accumulator + scoreObject.value; 
        }
        return accumulator + 0;
    }, 0);
}
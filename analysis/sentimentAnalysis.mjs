import sentiment from 'sentiment-french'

export default function(stringToTest){
    return sentiment(stringToTest).score;
}
import express from "express";
import reader from "../data/fileReader.mjs";
import {singleWordAnalysis, textAnalysis} from "../analysis/analysis.mjs";

export const router = express.Router();

/* GET home page. */
router.get('/', async(req, res, next) => {
  try{
    let text = await reader();
    res.render('index', { 
      title: 'La valeur des mots', 
      text: text,
      result: textAnalysis(text)
    });
  }catch (err){
    next(err);
  }
});
